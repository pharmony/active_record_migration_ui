module ActiveRecordMigrationUi
  class ActiveRecordMigrationUiChannel < ApplicationCable::Channel
    def subscribed
      stream_from ActiveRecordMigrationUi.ac_channel_name
    end

    def migrate(data)
      OrganiseMigratingScript.call(version: data['version'])
    end
  end
end
