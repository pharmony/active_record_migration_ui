require 'webpacker'
require 'webpacker/helper'

module ActiveRecordMigrationUi
  module ApplicationHelper
    include ::Webpacker::Helper

    def current_webpacker_instance
      ActiveRecordMigrationUi.webpacker
    end
  end
end
