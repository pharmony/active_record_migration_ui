require_dependency 'active_record_migration_ui/application_controller'

module ActiveRecordMigrationUi
  class MigrationsController < ApplicationController
    # Rack Middleware uses this method in order to run the index action.
    def self.call(env)
      action(:index).call(env)
    end

    def index
      respond_to do |format|
        format.html
        format.json do
          interactor = FindAllPendingMigrationScripts.call
          if interactor.failure?
            render json: interactor.errors, status: :unprocessable_entity
          else
            render json: interactor.scripts, status: :ok
          end
        end
      end
    end
  end
end
