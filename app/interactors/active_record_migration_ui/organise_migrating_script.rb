module ActiveRecordMigrationUi
  class OrganiseMigratingScript
    include Interactor::Organizer

    organize FindAllPendingMigrationScripts,
             EnsureMigrationVersionIsIncludedInPendingScripts,
             NotifyMigrationScriptAsRunning,
             MigrateMigrationScript,
             NotifyMigrationScriptAsDoneOrFail
  end
end
