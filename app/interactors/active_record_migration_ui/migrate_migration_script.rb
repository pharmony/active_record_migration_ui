module ActiveRecordMigrationUi
  class MigrateMigrationScript
    include Interactor

    def call
      sanity_checks!

      migration_paths = ActiveRecord::Migrator.migrations_paths
      migration_context = ActiveRecord::MigrationContext.new(migration_paths)

      # Lets the logger broadcasting the logs to the UI
      ActiveRecordMigrationUi.running_migration = true

      # In case something went wrong in the migration script, an exception is
      # thrown
      migration_context.migrate(context.version.to_i)

      # Preents the logger to broadcast the logs to the UI
      ActiveRecordMigrationUi.running_migration = false

      context.final_state = 'up'
    rescue StandardError => error
      Rails.logger.warn "Migration of #{context.version} failed: " \
                        "#{error.message}"
      context.final_state = 'failed'
    end

    private

    def sanity_checks!
      unless context.version
        context.fail!(errors: { version: 'is missing' })
      end

      return if context.version =~ /\d+/

      context.fail!(errors: {
        version: 'is invalid (Expected numbers like 20180323142544).'
      })
    end
  end
end
