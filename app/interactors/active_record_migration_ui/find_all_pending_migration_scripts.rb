module ActiveRecordMigrationUi
  class FindAllPendingMigrationScripts
    include Interactor

    def call
      context.scripts = build_pending_scripts_list
    end

    private

    def migrations_status
      # This will ensure the `schema_migrations` table exists
      ActiveRecord::SchemaMigration.create_table

      ActiveRecord::Base.connection.migration_context.migrations_status
    end

    def build_pending_scripts_list
      migrations_status.map do |status, version, name|
        next unless status == 'down'

        {
          name: name,
          version: version
        }
      end.compact
    end
  end
end
