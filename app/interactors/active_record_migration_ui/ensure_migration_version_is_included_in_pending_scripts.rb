module ActiveRecordMigrationUi
  class EnsureMigrationVersionIsIncludedInPendingScripts
    include Interactor

    def call
      sanity_checks!

      return if version_is_included_in_pending_scripts?

      context.fail!(errors: {
        version: 'was not found in pending migration script list'
      })
    end

    private

    def sanity_checks!
      unless context.version
        context.fail!(errors: { version: 'is missing' })
      end

      unless context.version =~ /\d+/
        context.fail!(errors: {
          version: 'is invalid (Expected numbers like 20180323142544).'
        })
      end

      unless context.scripts
        context.fail!(errors: { scripts: 'is missing' })
      end

      unless context.scripts.is_a?(Array)
        context.fail!(errors: {
          scripts: "must be an Array but it #{context.scripts.class.name}"
        })
      end

      return unless context.scripts.size.zero?

      context.fail!(errors: {
        scripts: 'must contain migration scripts, but is empty'
      })
    end

    def version_is_included_in_pending_scripts?
      context.scripts.detect { |script| script[:version] == context.version }
    end
  end
end
