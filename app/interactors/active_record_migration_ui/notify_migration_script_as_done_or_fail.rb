module ActiveRecordMigrationUi
  class NotifyMigrationScriptAsDoneOrFail
    include Interactor

    def call
      sanity_checks!

      # Emit a message through the WebSocket to notify the front that the given
      # migration script version changes the state to the given final_state.
      ActionCable.server.broadcast ActiveRecordMigrationUi.ac_channel_name,
                                   command: 'migrate',
                                   version: context.version,
                                   new_status: context.final_state
    end

    private

    def sanity_checks!
      unless context.version
        context.fail!(errors: { version: 'is missing' })
      end

      unless context.version =~ /\d+/
        context.fail!(errors: {
          version: 'is invalid (Expected numbers like 20180323142544).'
        })
      end

      return if context.final_state

      context.fail!(errors: { final_state: 'is missing' })
    end
  end
end
