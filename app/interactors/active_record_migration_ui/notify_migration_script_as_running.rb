module ActiveRecordMigrationUi
  class NotifyMigrationScriptAsRunning
    include Interactor

    def call
      sanity_checks!

      # Emit a message through the WebSocket to notify the front that the given
      # migration script version changes the state to running, which will show
      # a loader next to the migration script.
      ActionCable.server.broadcast ActiveRecordMigrationUi.ac_channel_name,
                                   command: 'migrate',
                                   version: context.version,
                                   new_status: 'running'
    end

    private

    def sanity_checks!
      unless context.version
        context.fail!(errors: { version: 'is missing' })
      end

      return if context.version =~ /\d+/

      context.fail!(errors: {
        version: 'is invalid (Expected numbers like 20180323142544).'
      })
    end
  end
end
