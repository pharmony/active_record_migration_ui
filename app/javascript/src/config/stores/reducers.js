import { combineReducers } from 'redux'

import { actionCable } from '../../action_cable/reducers'
import { app } from '../../app/reducers'

const rootReducer = combineReducers({
  actionCable,
  app
})

export default rootReducer
