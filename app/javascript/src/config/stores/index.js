import { applyMiddleware, createStore } from 'redux'
import { createLogger } from 'redux-logger'
import { createLogicMiddleware } from 'redux-logic'
import thunkMiddleware from 'redux-thunk'

import { asyncDispatchMiddleware } from './async-dispatch-middleware'

import logics from './logics'
import rootReducer from './reducers'

// ~~~~ Redux Logger ~~~~
const loggerMiddleware = createLogger()
// ~~~~  ~~~~

// ~~~~ Redux Logic ~~~~
// injected dependencies or constants for logic
const deps = {
}
const logicMiddleware = createLogicMiddleware(logics, deps)
// ~~~~  ~~~~

const store = createStore(
  rootReducer,
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware,
    logicMiddleware,
    asyncDispatchMiddleware
  )
)

export default store
