import {
  detectMigrationScriptHasFinish,
  migrateGivenScript,
  migrateNextScript
} from '../../migration/logics'

const logics = [
  detectMigrationScriptHasFinish,
  migrateGivenScript,
  migrateNextScript
]

export default logics
