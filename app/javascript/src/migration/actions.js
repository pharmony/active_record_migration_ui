import { migrationConstants } from './constants'

const scriptMigrateFinsihed = (version, new_status) => {
  const success = version => ({
    type: migrationConstants.MIGRATION_SCRIPT_FINISHED_ON_SUCCESS,
    version
  })
  const failure = version => ({
    type: migrationConstants.MIGRATION_SCRIPT_FINISHED_ON_FAILURE,
    version
  })

  return dispatch => {
    if (new_status === 'up') {
      dispatch(success(version))
    } else {
      dispatch(failure(version))
    }
  }
}

export const migrationActions = {
  scriptMigrateFinsihed
}
