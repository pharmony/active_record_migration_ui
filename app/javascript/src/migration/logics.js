import { createLogic } from 'redux-logic'

import { actionCableActions } from '../action_cable/actions'
import { actionCableConstants } from '../action_cable/constants'
import { appConstants } from '../app/constants'
import { migrationActions } from './actions'
import { migrationConstants } from './constants'

export const detectMigrationScriptHasFinish = createLogic({
  type: actionCableConstants.DATA_RECEIVED,

  process({ action }, dispatch, done) {
    if (action.data.command === 'migrate') {
      if (['up', 'failed'].includes(action.data.new_status)) {
        dispatch(migrationActions.scriptMigrateFinsihed(
          action.data.version,
          action.data.new_status
        ))
      }
    }

    done()
  }
})

export const migrateGivenScript = createLogic({
  type: migrationConstants.MIGRATION_REQUEST,

  process({ action }, dispatch, done) {
    dispatch(actionCableActions.send({
      command: 'migrate',
      data: {
        version: action.script.version
      }
    }))

    done()
  }
})

export const migrateNextScript = createLogic({
  type: [
    migrationConstants.RETRY_FAILED_MIGRATION_SCRIPTS,
    migrationConstants.MIGRATION_SCRIPT_FINISHED_ON_SUCCESS
  ],

  process({ getState }, dispatch, done) {

    const { scripts } = getState().app
    const nextMigration = scripts.find(script => script.status == 'pending')

    if (nextMigration) {
      // Continue migrating while there are pending migration scripts
      dispatch({
        script: nextMigration,
        type: migrationConstants.MIGRATION_REQUEST
      })
    } else {
      // When it's finished, determines if there were failures or not
      // and then dispatch the right action.
      const hadFailure = scripts.find(script => script.status === 'failed')
      let actionType = null

      if (hadFailure) {
        actionType = appConstants.START_ROLLING_MIGRATE_FAILURE
      } else {
        actionType = appConstants.START_ROLLING_MIGRATE_SUCCESS
      }

      dispatch({
        type: actionType
      })
    }

    done()
  }
})
