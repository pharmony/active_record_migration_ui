import { actionCableService } from '../action_cable/services'

const migrate = (version) => actionCableService.migrate(version)

export const migrationService = {
  migrate
}
