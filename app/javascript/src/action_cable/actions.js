import { actionCableConstants } from './constants'
import { actionCableService } from './services'

const connected = (channel) => {
  return dispatch => {
    dispatch({
      channel,
      type: actionCableConstants.CONNECTED
    })
  }
}

const disconnected = () => {
  return dispatch => {
    dispatch({
      type: actionCableConstants.DISCONNECTED
    })
  }
}

const onError = (error) => {
  return dispatch => {
    dispatch({
      error,
      type: actionCableConstants.ERROR
    })
  }
}

const onReceive = (data) => {
  return dispatch => {
    dispatch({
      data,
      type: actionCableConstants.DATA_RECEIVED
    })
  }
}

const create = () => {
  const request = () => ({
    type: actionCableConstants.CREATE_REQUEST
  })
  const success = (cableUrl) => ({
    cableUrl,
    type: actionCableConstants.CREATE_SUCCESS
  })
  const failure = (error) => ({
    error,
    type: actionCableConstants.CREATE_FAILURE
  })

  return dispatch => {
    dispatch(request())

    actionCableService.createAndSubscribe(dispatch, connected, disconnected, onError, onReceive)
      .then((cableUrl) => dispatch(success(cableUrl)))
      .catch(error => dispatch(failure(error)))
  }
}

const send = ({ command, data }) => {
  const request = () => ({
    type: actionCableConstants.SEND_REQUEST
  })
  const success = () => ({
    type: actionCableConstants.SEND_SUCCESS
  })
  const failure = (error) => ({
    error,
    type: actionCableConstants.SEND_FAILURE
  })

  return (dispatch, getState) => {
    dispatch(request())

    const { channel } = getState().actionCable

    actionCableService.send(channel, command, data)
      .then(() => dispatch(success()))
      .catch(error => dispatch(failure(error)))
  }
}

export const actionCableActions = {
  create,
  send
}
