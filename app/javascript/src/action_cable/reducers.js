import dotProp from 'dot-prop-immutable'

import { actionCableConstants } from './constants'

const initialState = {
  cableUrl: null,
  channel: null,
  // Connection
  connected: false,
  connecting: false,
  disconnected: false,
  error: null,
  // Send
  commandSent: false,
  sendingCommand: false,
  sendingCommandFailed: false
}

export const actionCable = (state = initialState, action) => {
  let newState = null

  switch (action.type) {
    /*
    ** ActionCable creation
    */
    case actionCableConstants.CREATE_REQUEST:
      newState = dotProp.set(state, 'cableUrl', null)
      newState = dotProp.set(newState, 'channel', null)
      newState = dotProp.set(newState, 'connected', false)
      newState = dotProp.set(newState, 'connecting', true)
      return dotProp.set(newState, 'disconnected', false)
    case actionCableConstants.CREATE_SUCCESS:
      return dotProp.set(state, 'cableUrl', action.cableUrl)
    case actionCableConstants.CREATE_FAILURE:
      newState = dotProp.set(state, 'cableUrl', null)
      return dotProp.set(newState, 'channel', null)

    /*
    ** ActionCable subscription
    */
    case actionCableConstants.CONNECTED:
      newState = dotProp.set(state, 'channel', action.channel)
      newState = dotProp.set(newState, 'connected', true)
      newState = dotProp.set(newState, 'connecting', false)
      return dotProp.set(newState, 'disconnected', false)
    case actionCableConstants.DISCONNECTED:
      newState = dotProp.set(state, 'connected', false)
      newState = dotProp.set(newState, 'connecting', false)
      return dotProp.set(newState, 'disconnected', true)
    case actionCableConstants.ERROR:
      newState = dotProp.set(state, 'connected', false)
      newState = dotProp.set(newState, 'connecting', false)
      newState = dotProp.set(newState, 'disconnected', true)
      return dotProp.set(newState, 'error', action.error)

    /*
    ** ActionCable Send commands
    */
    case actionCableConstants.SEND_REQUEST:
      newState = dotProp.set(state, 'commandSent', false)
      newState = dotProp.set(newState, 'sendingCommand', true)
      return dotProp.set(newState, 'sendingCommandFailed', false)
    case actionCableConstants.SEND_SUCCESS:
      newState = dotProp.set(state, 'commandSent', true)
      newState = dotProp.set(newState, 'sendingCommand', false)
      return dotProp.set(newState, 'sendingCommandFailed', false)
    case actionCableConstants.SEND_FAILURE:
      newState = dotProp.set(state, 'commandSent', true)
      newState = dotProp.set(newState, 'sendingCommand', false)
      return dotProp.set(newState, 'sendingCommandFailed', true)
    default:
      return state
  }
}
