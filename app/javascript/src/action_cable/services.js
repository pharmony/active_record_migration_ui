import ActionCable from 'actioncable'

const channelName = () => 'ActiveRecordMigrationUi::ActiveRecordMigrationUiChannel'

// ActionCable.startDebugging()

const createAndSubscribe = (dispatch, connected, disconnected, onError, onReceive) => {
  return new Promise((resolve, reject) => {
    try {
      const cableUrl = document.querySelector('meta[name="action-cable-url"]').content
      // `armui=true` allows you to detect this gem's requests.
      // Could be useful for authorisations.
      const cable = ActionCable.createConsumer(cableUrl + '?armui=true')

      cable.connection.events.error = error => dispatch(onError(error))

      const channel = cable.subscriptions.create(channelName(), {
        connected: () => dispatch(connected(channel)),
        disconnected: () => dispatch(disconnected()),
        received: (data) => dispatch(onReceive(data))
      })

      resolve(cableUrl)
    } catch(error) {
      reject(error)
    }
  })
}

const send = (channel, command, data) => {
  return new Promise((resolve, reject) => {
    try {
      channel.perform(command, data)
      resolve()
    } catch(error) {
      reject(error)
    }
  })
}

export const actionCableService = {
  createAndSubscribe,
  send
}
