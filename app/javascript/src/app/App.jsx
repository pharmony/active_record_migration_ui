import React from 'react'
import { connect } from 'react-redux'
import { Alert, Button } from 'reactstrap'

import { appActions } from './actions'
import Loader from './Loader'
import MigrationScriptList from './MigrationScriptList'

class App extends React.Component {
  componentDidMount() {
    const { dispatch } = this.props
    dispatch(appActions.fetchPendingMigrationScripts())
  }

  goBackToTheApp() {
    window.location.reload()
  }

  migrateNow() {
    const { dispatch } = this.props
    dispatch(appActions.startRollingMigrate())
  }

  retryMigrateNow() {
    const { dispatch } = this.props
    dispatch(appActions.retryFailedMigrate())
  }

  render() {
    const {
      actionCableDisconnected,
      backToAppButtonVisible,
      error,
      loading,
      migrateButtonDisabled,
      migrateButtonVisible,
      migrationFailed,
      retryButtonDisabled,
      retryButtonVisible,
      scripts
    } = this.props

    return (
      <div className="container d-flex justify-content-center align-items-center h-100">
        <div className="d-flex flex-column w-100">
          <div className="d-flex flex-row justify-content-center">
            <h1>ActiveRecord Migration UI</h1>
          </div>
          <div className="d-flex flex-row justify-content-center">
            <p className="lead">You have pending migration scripts!</p>
          </div>
          <div className="d-flex flex-column justify-content-center align-items-center overflow-auto">
            {loading && (
              <Loader />
            )}

            {actionCableDisconnected && (
              <Alert color="danger" className="d-flex">
                WebSocket connection failed, please check your WebSocket server.
              </Alert>
            )}

            {error && (
              <Alert color="danger" className="d-flex">
                Something went wrong while fetching the pending migrations.
              </Alert>
            )}

            {migrationFailed && (
              <Alert color="danger" className="d-flex">
                Something went wrong while running a migration script.
              </Alert>
            )}

            {Array.isArray(scripts) && scripts.length === 0 && (
              <Alert color="warning" className="d-flex">
                No pending migration scripts found.
                Did you ran manually the `rails db:migrate` command?
              </Alert>
            )}

            {Array.isArray(scripts) && scripts.length > 0 && (
              <React.Fragment>
                <div className="d-flex flex-row w-100">
                  <div
                    className="d-flex flex-row flex-fill overflow-auto mt-5"
                    style={{ maxHeight: '70vh' }}
                  >
                    <MigrationScriptList scripts={scripts} />
                  </div>
                </div>
                <div className="d-flex flex-column align-items-end mt-3 w-100">
                  {migrateButtonVisible && (
                    <Button
                      className="mr-3"
                      color="primary"
                      onClick={() => this.migrateNow()}
                      disabled={migrateButtonDisabled || actionCableDisconnected}
                    >
                      Migrate now!
                    </Button>
                  )}
                  {retryButtonVisible && (
                    <Button
                      className="mr-3"
                      color="danger"
                      onClick={() => this.retryMigrateNow()}
                      disabled={retryButtonDisabled || actionCableDisconnected}
                    >
                      Retry failed scripts
                    </Button>
                  )}
                  {backToAppButtonVisible && (
                    <Button
                      className="mr-3"
                      color="success"
                      onClick={() => this.goBackToTheApp()}
                    >
                      Go back to my app!
                    </Button>
                  )}
                </div>
              </React.Fragment>
            )}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({
  actionCable: { disconnected: actionCableDisconnected },
  app: {
    backToAppButtonVisible,
    error,
    loading,
    migrateButtonDisabled,
    migrateButtonVisible,
    migrationFailed,
    retryButtonDisabled,
    retryButtonVisible,
    scripts
  }
}) => ({
  actionCableDisconnected,
  backToAppButtonVisible,
  error,
  loading,
  migrateButtonDisabled,
  migrateButtonVisible,
  migrationFailed,
  retryButtonDisabled,
  retryButtonVisible,
  scripts
})

export default connect(mapStateToProps)(App)
