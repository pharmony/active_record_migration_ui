import { appConstants } from './constants'
import { appService } from './services'
import { migrationConstants } from '../migration/constants'

const fetchPendingMigrationScripts = () => {
  const request = () => ({
    type: appConstants.FETCH_PENDING_MIGRATION_SCRIPTS_REQUEST
  })
  const success = (scripts) => ({
    scripts,
    type: appConstants.FETCH_PENDING_MIGRATION_SCRIPTS_SUCCESS
  })
  const failure = (error) => ({
    error,
    type: appConstants.FETCH_PENDING_MIGRATION_SCRIPTS_FAILURE
  })

  return dispatch => {
    dispatch(request())

    appService.fetchPendingMigrationScripts()
      .then(scripts => dispatch(success(scripts)))
      .catch(error => dispatch(failure(error)))
  }
}

const retryFailedMigrate = () => {
  const request = () => ({ type: appConstants.RETRY_FAILED_SCRIPTS })

  return dispatch => {
    dispatch(request())
  }
}

const startRollingMigrate = () => {
  const request = () => ({ type: appConstants.START_ROLLING_MIGRATE_BEGIN })
  const migrationRequest = (script) => ({
    script,
    type: migrationConstants.MIGRATION_REQUEST
  })

  return (dispatch, getState) => {
    dispatch(request())

    const { scripts } = getState().app

    const nextMigration = scripts.find(script => script.status == 'pending')
    // See app/migration/logics.js
    dispatch(migrationRequest(nextMigration))
  }
}

export const appActions = {
  fetchPendingMigrationScripts,
  retryFailedMigrate,
  startRollingMigrate
}
