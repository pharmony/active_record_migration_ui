import dotProp from 'dot-prop-immutable'

import { actionCableConstants } from '../action_cable/constants'
import { appConstants } from './constants'
import { migrationConstants } from '../migration/constants'

const initialState = {
  backToAppButtonVisible: false,
  error: null,
  loading: false,
  migrateButtonDisabled: false,
  migrateButtonVisible: false,
  migrationFailed: false,
  retryButtonDisabled: false,
  retryButtonVisible: false,
  runningRollingUpdate: false,
  runningScriptVersion: null,
  scripts: null
}

const findScriptPositionBy = (scripts, options = {}) => {
  if (options.version) {
    return scripts.findIndex(script => script.version === options.version)
  }

  return null
}

const isAScriptUpdateMessage = (action) => {
  if (action.data && action.data.command === 'migrate') {
    return action.data.version && action.data.new_status
  } else {
    return false
  }
}

const isALogMessage = action => action.data && action.data.command === 'log'

export const app = (state = initialState, action) => {
  let newState = null

  switch (action.type) {
    case appConstants.FETCH_PENDING_MIGRATION_SCRIPTS_REQUEST:
      return dotProp.set(state, 'loading', true)
    case appConstants.FETCH_PENDING_MIGRATION_SCRIPTS_SUCCESS:
      newState = dotProp.set(state, 'loading', false)
      newState = dotProp.set(newState, 'migrateButtonVisible', true)
      action.scripts.map(script => {
        script.status = 'pending'
        script.logs = null
      })
      return dotProp.set(newState, 'scripts', action.scripts)
    case appConstants.FETCH_PENDING_MIGRATION_SCRIPTS_FAILURE:
      newState = dotProp.set(state, 'loading', false)
      return dotProp.set(newState, 'error', action.error)

    /*
    ** Disables the "Migrate now!" button state after clicking it.
    */
    case appConstants.START_ROLLING_MIGRATE_BEGIN:
      newState = dotProp.set(state, 'migrateButtonDisabled', true)
      return dotProp.set(newState, 'runningRollingUpdate', true)
    case appConstants.START_ROLLING_MIGRATE_FAILURE:
      newState = dotProp.set(state, 'migrateButtonDisabled', false)
      newState = dotProp.set(newState, 'migrateButtonVisible', false)
      newState = dotProp.set(newState, 'retryButtonVisible', true)
      return dotProp.set(newState, 'runningRollingUpdate', false)
    case appConstants.START_ROLLING_MIGRATE_SUCCESS:
      newState = dotProp.set(state, 'backToAppButtonVisible', true)
      newState = dotProp.set(newState, 'migrateButtonDisabled', false)
      newState = dotProp.set(newState, 'migrateButtonVisible', false)
      return dotProp.set(newState, 'runningRollingUpdate', false)

    case migrationConstants.MIGRATION_REQUEST:
      return dotProp.set(state, 'runningScriptVersion', action.script.version)

    case migrationConstants.MIGRATION_SCRIPT_FINISHED_ON_FAILURE:
      return dotProp.set(state, 'migrationFailed', true)

    /*
    ** Manage script status update from ActionCable
    */
    case actionCableConstants.DATA_RECEIVED:
      // Script status update
      if (isAScriptUpdateMessage(action)) {
        const scriptIndex = findScriptPositionBy(state.scripts, {
          version: action.data.version
        })

        return dotProp.set(
          state,
          `scripts.${scriptIndex}.status`,
          action.data.new_status
        )
      } else if (isALogMessage(action)) {
        const { runningScriptVersion } = state
        // Script logs update
        const scriptIndex = state.scripts.findIndex(script => (
          script.version === runningScriptVersion
        ))

        const script = state.scripts.find(script => (
          script.version === runningScriptVersion
        ))

        return dotProp.set(
          state,
          `scripts.${scriptIndex}.logs`,
          [...script.logs || [], action.data.message]
        )
      } else {
        return state
      }

    /*
    ** Data reset
    */
    case appConstants.RETRY_FAILED_SCRIPTS: {
        newState = state

        const failed = state.scripts.filter(script => script.status == 'failed')

        failed.map(failedScript => {
          const scriptIndex = state.scripts.findIndex(script => {
            return script.version === failedScript.version
          })

          newState = dotProp.set(
            newState,
            `scripts.${scriptIndex}.status`,
            'pending'
          )
        })

        action.asyncDispatch({
          type: migrationConstants.RETRY_FAILED_MIGRATION_SCRIPTS
        })

        return newState
      }
    default:
      return state
  }
}
