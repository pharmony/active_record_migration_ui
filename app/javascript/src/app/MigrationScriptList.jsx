import React from 'react'
import {
  Badge,
  Collapse,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText
} from 'reactstrap'
import uuidv1 from 'uuid/v1'

import MigrationScriptItem from './MigrationScriptItem'

const MigrationScriptList = (props) => {
  const { scripts } = props

  return (
    <ListGroup className="flex-fill">
      {scripts.map((script) => (
        <MigrationScriptItem script={script} key={uuidv1()} />
      ))}
    </ListGroup>
  )
}

export default MigrationScriptList
