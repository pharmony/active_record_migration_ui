import React from 'react'

const Loader = props => {
  return (
    <React.Fragment>
      <div className="d-flex mr-3">
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>
      <div className="d-flex">
        <span>Loading the pending migration scripts ...</span>
      </div>
    </React.Fragment>
  )
}

export default Loader
