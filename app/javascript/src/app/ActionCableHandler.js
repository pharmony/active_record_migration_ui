import React from 'react'
import { connect } from 'react-redux'

import { actionCableActions } from '../action_cable/actions'

class ActionCableHandler extends React.Component {
  componentDidMount() {
    const { dispatch } = this.props
    dispatch(actionCableActions.create())
  }

  render() {
    return null
  }
}

export default connect()(ActionCableHandler)
