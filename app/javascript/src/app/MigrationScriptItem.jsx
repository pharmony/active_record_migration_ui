import React from 'react'
import { connect } from 'react-redux'
import {
  Badge,
  Collapse,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  ListGroupItemText
} from 'reactstrap'
import uuidv1 from 'uuid/v1'

class MigrationScriptItem extends React.Component {
  state = {
    logs: null,
    logOpen: false,
    version: null
  }

  componentDidMount = () => {
    const { script } = this.props

    this.setState({
      version: script.version
    })
  }

  componentDidUpdate = () => {
    const { scripts } = this.props
    const { logs, version } = this.state

    const script = scripts.find(script => script.version === version)

    if ((script.logs && logs === null) ||
        (script.logs && script.logs.length > logs.length))
    {
      this.setState({
        logs: script.logs
      })
    }
  }

  badgeColor = (status) => {
    switch(status) {
      case 'pending':
        return 'warning'
      case 'failed':
        return 'danger'
      case 'up':
        return 'success'
      default:
        return 'warning'
    }
  }

  toggleOpenLog = () => {
    const { logs, logOpen } = this.state

    if (!logs) { return }

    this.setState({
      logOpen: !logOpen
    })
  }

  render() {
    const { script } = this.props
    const { logOpen, logs } = this.state

    return (
      <ListGroupItem onClick={() => this.toggleOpenLog()} tag={logs ? 'a' : 'li'} href="#">
        <ListGroupItemHeading className="mb-0">
          <div className="badge-container text-center">
            <Badge className="migration-status" color={this.badgeColor(script.status)}>{script.status}</Badge>
          </div>
          <span className="ml-5 migration-version">{script.version}</span>
          <span className="ml-5 migration-name">{script.name}</span>
        </ListGroupItemHeading>
        <Collapse isOpen={logOpen}>
          <div className="mb-0">
            <div className="migration-log-container overflow-auto my-2 px-3">
              {logs && logs.map(log => (
                <p className="mb-0" key={uuidv1()}>{log}</p>
              ))}
            </div>
          </div>
        </Collapse>
      </ListGroupItem>
    )
  }
}

const mapStateToProps = (state) => {
  const { scripts } = state.app

  return { scripts }
}

export default connect(mapStateToProps)(MigrationScriptItem)
