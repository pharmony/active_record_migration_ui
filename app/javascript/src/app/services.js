import { ApiUtils } from '../helpers/ApiUtils'
import { FormCsrf } from '../helpers/FormCsrf'

const fetchPendingMigrationScripts = () => {
  return fetch('/migrations', {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  })
  .then(ApiUtils.checkStatus)
  .then(response => response.json())
}

const migrateScript = (version) => {
  return fetch(`/migrations/${version}/migrate`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'X-CSRF-Token': FormCsrf.csrfToken()
    }
  })
  .then(ApiUtils.checkStatus)
  .then(response => response.json())
}

export const appService = {
  fetchPendingMigrationScripts,
  migrateScript
}
