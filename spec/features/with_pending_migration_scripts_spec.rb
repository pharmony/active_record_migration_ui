require 'rails_helper'
require 'rails/generators'
require 'rails/generators/active_record/migration/migration_generator'

feature 'With pending migration scripts' do
  scenario 'Have one pending migration script, check the migration list' do
    given_a_new_migration_script_exists
    when_i_access_the_app

    expect(page).to be_the_active_record_migration_ui_gem
    expect(page).to have_css('ul.list-group li.list-group-item', count: 1)
    within('ul.list-group li.list-group-item') do
      expect(page).to have_css('span.migration-status', text: 'pending')
      expect(page).to have_css('span.migration-name', text: 'A dummy script')
    end
  end

  def given_a_new_migration_script_exists
    given_the_db_migrate_folder_has_been_reseted

    Rails.application.config.paths['db/migrate'] = the_db_migrate_folder
    ActiveRecord::Generators::MigrationGenerator.start ['a_dummy_script']
  end
end
