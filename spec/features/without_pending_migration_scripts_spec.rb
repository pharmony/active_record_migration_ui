require 'rails_helper'

feature 'Without pending migration scripts' do
  scenario "Checks the ActiveRecord Migration UI gem doesn't show up"  do
    given_no_migration_script_exist
    when_i_access_the_app
    then_i_should_see_the_dummy_app
  end

  def then_i_should_see_the_dummy_app
    expect(page).to be_the_dummy_app
  end
end
