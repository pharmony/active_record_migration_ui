# frozen_string_literal: true

#
# This collects the Selenium browser's console logs and raise the
# `DriverJSError` exception so that they are visible on a failure.
#
class DriverJSError < StandardError; end

RSpec.configure do |config|
  config.after(:all) do
    errors = page.driver.browser.manage.logs.get(:browser)
                 .select { |log| log.level == 'SEVERE' && log.message.present? }
                 .map(&:message)
                 .to_a

    raise DriverJSError, errors.join("\n\n") if errors.present?
  end
end
