#
# Look in the page for a h1 tag with the "ActiveRecord Migration UI" text.
#
module BeTheActiveRecordMigrationUiGem
  extend RSpec::Matchers::DSL

  PAGE_TITLE = 'ActiveRecord Migration UI'.freeze

  matcher :be_the_active_record_migration_ui_gem do
    match do |page|
      begin
        page.find('h1', text: PAGE_TITLE)
        true
      rescue Capybara::ElementNotFound
        false
      end
    end
    failure_message do |page|
      "expected #{page.body} to have a h1 tag with the text " \
      "\"#{PAGE_TITLE}\" but did not"
    end
  end
end
