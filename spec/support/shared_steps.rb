def the_db_migrate_folder
  File.join(DUMMY_APP_ROOT_FOLDER, 'db', 'migrate')
end

def given_no_migration_script_exist
  FileUtils.rm_rf the_db_migrate_folder
end

def given_the_db_migrate_folder_exists
  FileUtils.mkdir_p the_db_migrate_folder
end

def given_the_db_migrate_folder_has_been_reseted
  given_no_migration_script_exist
  given_the_db_migrate_folder_exists
end

def when_i_access_the_app
  visit '/'
end
