#
# Look in the page for a h1 tag with the "The dummy app!" text.
#
module BeTheDummyApp
  extend RSpec::Matchers::DSL

  PAGE_TITLE = 'The dummy app!'.freeze

  matcher :be_the_dummy_app do
    match do |page|
      Capybara.string(page.body).has_selector?('h1', text: PAGE_TITLE)
    end
    failure_message do |page|
      "expected #{page.body} to have a h1 tag with the text " \
      "\"#{PAGE_TITLE}\" but did not"
    end
  end
end
