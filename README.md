# ActiveRecordMigrationUi [![pipeline status](https://gitlab.com/pharmony/active_record_migration_ui/badges/master/pipeline.svg)](https://gitlab.com/pharmony/active_record_migration_ui/commits/master)

This gem replaces the Rails default pending migration page with a small React
app which allows you to run your migration scripts from your web browser.

So before this gem you had this :

![Rails default pending migration page](screenshots/rails-default-pending-migration-page.png)

After having installed this gem you will have this :

![ActiveRecord Migration UI page](screenshots/ar-migration-ui-page.png)

![ActiveRecord Migration UI script logs](screenshots/ar-migration-ui-logs.png)

## Usage

The ActiveRecord Migration UI page will show up as soon as you installed the gem,
restarted your server and have at least one pending migration script.

Click the "Migrate now!" button to run the pending migration scripts.

## Installation

Add this gem where you want to it to be available.

It is recommended to use it only on your environment machine, but you could also
deploy it to your staging environment for instance.

Add this line to your application's Gemfile:

```ruby
group :development do
  gem 'active_record_migration_ui'
end
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install active_record_migration_ui
```

## Development

1. Install [docker-sync](http://docker-sync.io): `gem install docker-sync`
2. Build the Docker image: `docker-compose build webpack`
3. Boot webpack: `docker-sync-stack start`
  This step will fail with the error:
  ```
  error Command "webpack-dev-server" not found.
  ```
  _TODO : find a way to not have this error_
4. Install NPM packages
  ```
  $ docker-compose run --rm -v armui-sync:/gem:nocopy webpack yarn
  ```
5. Restart again docker-sync: `docker-sync-stack start`

Now you should see webpack running on port 3036.

### When you need SSL

In this case you have to update the `config/webpacker.yml` file in order to update the `https: false` attribute to:

```yaml
development:
  # ...
  dev_server:
    # ...
    https:
      key: |
        -----BEGIN PRIVATE KEY-----
        -----END PRIVATE KEY-----
      cert: |
        -----BEGIN CERTIFICATE-----
        -----END CERTIFICATE-----
      ca: |
        -----BEGIN CERTIFICATE-----
        -----END CERTIFICATE-----
```

Don't forget to restart the webpack container.

## Running the tests

After having followed the (Development)[#development] steps, you have a `webpack`
container (and a `selenium` one).

You first need to prepare the JavaScript app for the tests, as if it was
released :

```
$ docker-compose exec webpack bash -c 'RAILS_ENV=production bin/webpack'
```

When this is done, you are ready to execute the tests :

```
$ docker-compose exec webpack rake
```

This command will run [Rspec](https://rspec.info) and [Capybara](http://teamcapybara.github.io/capybara/)
within [Selenium](https://www.seleniumhq.org) which allows to run the JavaScript
app in a real web browser.

To debug from this web browser, you can have a look at the `docker-compose.yml`
file, and change the `selenium` service from:

```yaml
services:
  selenium:
    image: selenium/standalone-chrome:3.141.59-palladium
    # image: selenium/standalone-chrome-debug:3.141.59-palladium
    # ports:
    #   - "5900:5900"
    volumes:
      - /dev/shm:/dev/shm
  ...
```

to

```yaml
services:
  selenium:
    # image: selenium/standalone-chrome:3.141.59-palladium
    image: selenium/standalone-chrome-debug:3.141.59-palladium
    ports:
      - "5900:5900"
    volumes:
      - /dev/shm:/dev/shm
  ...
```

Then restart the stack and you can now connect via VNC to `vnc://localhost:5900`
and use the `secret` password.

## Architecture

We made an `ARCHITECURE.md` file in order to guide you through this gem's code.

## Contributing

1. Fork this gem
2. Implement your new features
3. [Clean your branch history](https://thoughtbot.com/blog/git-interactive-rebase-squash-amend-rewriting-history)
4. Open a Merge Request

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Releasing a new version

1. Run the `docker-sync-stack start` command (See [Development](#development)).
2. Run `docker-compose run --rm -v armui-sync:/gem:nocopy webpack bash -c 'RAILS_ENV=production RAILS_MASTER_KEY=4d046dc285e33d0750e78d7effe25f3b rails build'`

This will produce the gem in the `pkg/` folder.
