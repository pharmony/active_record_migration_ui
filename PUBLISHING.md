# Publishing a new release

1. Update the `CHANGELOG.md` file in order to move all the items from the **Unreleased** into a new version (See existing versions in the `CHANGELOG.md` file).
2. Update the `lib/active_record_migration_ui/version.rb` file accordingly
3. Commit the changes with the commit message `Bumped version v<version>`
4. Create a git tag `v<version>` on this commit and push it
5. Wait for the build and if it pass
6. Run the `rake release` command from the `webpack` container: `docker-compose exec webpack rake release`
