# active_record_migration_ui

## Unreleased



## 0.1.2 - (2019-09-09)

 - Updates migration page style
 - Disables the migration button and show an alert when WebSocket server is down (Closes #3)
 - Stops running migration scripts and show an error when a migration script fails (Closes #1)

## 0.1.1 - (2019-06-09)

 - Logs as a warning the error when a migration script fails
 - Passes the `armui=true` param to the ActionCable URL

## 0.1.0 - (2019-06-09)

 - Initial version
