$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'active_record_migration_ui/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = 'active_record_migration_ui'
  spec.version     = ActiveRecordMigrationUi::VERSION
  spec.authors     = ['Guillaume Hain']
  spec.email       = ['zedtux@zedroot.org']
  spec.homepage    = 'https://gitlab.com/pharmony/active_record_migration_ui'
  spec.summary     = 'A web UI to execute pending Rails migrations'
  spec.description = 'Shows a web view when you have pending migration ' \
                     'scripts and executes them for your automatically!'
  spec.license     = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata = {
      'allowed_push_host' => 'https://rubygems.org',
      'bug_tracker_uri'   => 'https://gitlab.com/pharmony/active_record_migration_ui/issues',
      'source_code_uri'   => 'https://gitlab.com/pharmony/active_record_migration_ui',
      'changelog_uri'     => 'https://gitlab.com/pharmony/active_record_migration_ui/blob/master/CHANGELOG.md'
    }
  else
    raise 'RubyGems 2.0 or newer is required to protect against public gem ' \
          'pushes.'
  end

  spec.files = Dir[
    '{app/channels,' \
    'app/controllers,' \
    'app/interactors,' \
    'app/views,' \
    'db,' \
    'lib/active_record_migration_ui,' \
    'public}/**/*',
    'config/routes.rb',
    'lib/active_record_migration_ui.rb',
    'MIT-LICENSE',
    'Rakefile',
    'README.md'
  ]

  spec.test_files = Dir["spec/**/*"]

  spec.required_ruby_version = '>= 2.2.0'

  spec.add_dependency 'actioncable', '>= 5'
  spec.add_dependency 'activerecord', '>= 5'
  spec.add_dependency 'interactor', '>= 3'
  spec.add_dependency 'railties', '>= 4.2'
  spec.add_dependency 'tzinfo-data'

  spec.add_development_dependency 'capybara', '~> 3.24.0'
  spec.add_development_dependency 'capybara-screenshot', '~> 1.0.23'
  spec.add_development_dependency 'database_cleaner', '~> 1.7.0'
  spec.add_development_dependency 'puma', '~> 3.12.1'
  spec.add_development_dependency 'rspec-rails', '~> 3.8.0'
  spec.add_development_dependency 'selenium-webdriver', '~> 3.142.3'
  spec.add_development_dependency 'sqlite3', '~> 1.4.1'
  spec.add_development_dependency 'webpacker', '~> 4.0.6'
end
