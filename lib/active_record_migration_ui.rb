# Rails
require 'action_cable/engine'

# External libraries
require 'interactor'

# Interanl files
require 'active_record_migration_ui/engine'
require 'active_record_migration_ui/logger'
require 'active_record_migration_ui/middleware'

module ActiveRecordMigrationUi
  ROOT_PATH = Pathname.new(File.join(__dir__, '..'))

  class << self
    attr_accessor :configuration
    attr_accessor :running_migration

    def ac_channel_class_name
      'ActiveRecordMigrationUi::ActiveRecordMigrationUiChannel'
    end

    def ac_channel_name
      'active_record_migration_ui'
    end

    def needs_migration?
      ActiveRecord::Base.connection.migration_context.needs_migration?
    end

    def running_migration?
      running_migration == true
    end

    def webpacker
      @webpacker ||= ::Webpacker::Instance.new(
        root_path: ROOT_PATH,
        config_path: ROOT_PATH.join('config', 'webpacker.yml')
      )
    end

    def webpacker_dev_server
      ActiveRecordMigrationUi.webpacker.config.dev_server
    rescue
      nil
    end
  end

  def self.init_config
    self.configuration ||= Configuration.new
  end

  def self.configure
    self.init_config
    yield(configuration)
  end

  class Configuration
    # Host where is running webpacker (Useful while developing)
    # It sets the `:host` attribute of the `javascript_pack_tag` helper.
    attr_accessor :webpacker_host
  end
end

ActiveRecordMigrationUi.init_config
