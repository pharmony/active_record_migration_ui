module ActiveRecordMigrationUi
  class Engine < ::Rails::Engine
    # Forces the engine name, otherwise it would be
    # `active_record_migration_ui_engine`.
    engine_name 'active_record_migration_ui'

    isolate_namespace ActiveRecordMigrationUi

    # Insert our Rack Middleware rescuing the pending migration error
    initializer 'active_record_migration_ui.configure_rails_initialization' do |app|
      # Don't use swap here otherwise an error is raised stating that the
      # ActiveRecord::Migration::CheckPending middleware is not present in the
      # dummy app stack.
      app.middleware.use ActiveRecordMigrationUi::Middleware
      app.middleware.delete ActiveRecord::Migration::CheckPending
    end

    # Auto-inject our routes to the host app
    initializer 'active_record_migration_ui.configuration' do |app|
      app.routes.append do
        mount ActiveRecordMigrationUi::Engine => '/active_record_migration_ui'
      end
    end

    # Configures Webpacker within the host app
    initializer 'webpacker.proxy' do |app|
      next unless ActiveRecordMigrationUi.webpacker_dev_server.present?

      app.middleware.insert_before(
        0,
        Webpacker::DevServerProxy,
        ssl_verify_none: true,
        webpacker: ActiveRecordMigrationUi.webpacker
      )
    end

    # Serves the engine's webpack when requested
    initializer 'webpacker.static' do |app|
      app.config.middleware.use(
        Rack::Static,
        urls: ['/ar-migration-ui-packs'],
        root: File.expand_path(File.join(__dir__, '..', '..', 'public'))
      )
    end

    # Adds the ActiveRecordMigrationUi::Logger as a second one so that the host
    # project continues to log in the normal file, and this gem captures the
    # ActiveRecord::Base.logger logs in order to show them in the app during the
    # database scripte migration execution.
    initializer 'active_record_logger.configuration' do
      ActiveRecord::Base.logger.extend(
        ActiveSupport::Logger.broadcast(
          ActiveRecordMigrationUi::Logger.new
        )
      )
    end
  end
end
