ActiveRecordMigrationUi::Engine.routes.draw do
  resources :migrations, only: %w[index migrate] do
    member do
      post :migrate
    end
  end
end
