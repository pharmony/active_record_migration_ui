# Architecture

The aim of this section is to guide you on the way this gem works so that you
can hack it faster.

## The engine

This gem is a [Rails Engine](https://guides.rubyonrails.org/engines.html), so
the `lib/active_record_migration_ui/engine.rb` file is where this gem starts.

This file is responsible to :

1. Replace the Rails `ActiveRecord::Migration::CheckPending` middleware with
this gem's middleware
2. Mount the routes file in the host app
3. _Connect_ this gem's webpack to the host app's webpack
4. Inject a custom logger that streams ActiveRecord::Base logs to the React app

## The middleware

This [Rack](https://rack.github.io) Middleware is mounted by the Engine.

Rack executes the middlewares one after the other in a specific order. You can
see the middlewares of your app with the `rake middleware` command.

The `lib/active_record_migration_ui/middleware.rb` file is well commented to
understand how it works, so please have a look at this file.

To summarise its role : It forward the request to the `MigrationsController`
when pending migrations have been detected, but lets passing the webpack and
Action Cable requests.

## The MigrationsController

This controller is used :

 - in HTML to load the React application
 - in JavaScript to return the list of pending migrations


## Action Cable

[Action Cable](https://guides.rubyonrails.org/action_cable_overview.html) is an
abstraction of the [WebSockets](https://en.wikipedia.org/wiki/WebSocket)
allowing bidirectional communication with the server.

This gem use it to request the migration of a given script, and to receive the
running logs.

The `app/channels/active_record_migration_ui/active_record_migration_ui_channel.rb`
 file defines the channel on the server side, and the
 `app/javascript/src/action_cable/` folder contains all the JavaScript code for
the UI part.

## Interactors

This gem uses the [interactor](https://github.com/collectiveidea/interactor) gem
which helps in isolating code.
See the [What is an interacor?](https://github.com/collectiveidea/interactor#what-is-an-interactor)
section.

Have a look at the OrganiseMigratingScript organiser where you'll see in which
order the interactors are used.
